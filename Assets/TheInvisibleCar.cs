﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheInvisibleCar : MonoBehaviour {
	private GameObject myCar;
	private PlayerMovement myMove;

	// Use this for initialization
	void Start () {
		myCar = GameObject.Find ("Car");
		//Find the Car Object
		myMove = GetComponent<PlayerMovement>();
		//Remove ability to drive with Car turned object turned off.
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.O)) {
			myCar.SetActive (false);
			myMove.enabled = !myMove.enabled;
		}
		if (Input.GetKeyUp (KeyCode.O)) {
			myCar.SetActive (true);
			myMove.enabled = !myMove.enabled;
			//BUG
			//Can Turn the Car off. Then Turn the car invisible and drive so long as you hold the "O" Key
		}
	}
}
