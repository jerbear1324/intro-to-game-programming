﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float speed = 0.0f;
	//public for designers
	public float rotationSpeed = 0.0f;
	//public for designers
	public float reverseSpeed = 0.0f;
	//public to allow for designers to alter it faster than having to go into the code
	//Debug for use of new things to make sure that they "should work"
	private Transform tf;
	private float nosSpeed = 0.0f;
	public float nsfast = 0.0f;
	public float nsnormal = 0.0f;
	//public SpriteRenderer sr;
	//SCRAPPED

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform>();
		//sr = GetComponent<SpriteRenderer>();
		//SCRAPPED
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W)) {
			tf.position += tf.forward * speed * nosSpeed;
		}
		//Move Forward
		//fixed rate of speed
		//not ideal
		if (Input.GetKey (KeyCode.A)) {
			tf.Rotate (Vector3.down, Time.deltaTime * rotationSpeed);
		}
		//turn left
		if (Input.GetKey (KeyCode.S)) {
			tf.position -= tf.forward * speed / reverseSpeed;
		}
		//Move backwards
		//Will move at speed divivded by the reverse speed to give
		//the impression that you are actually in reverse
		if (Input.GetKey (KeyCode.D)) {
			tf.Rotate (Vector3.up, Time.deltaTime * rotationSpeed);
		}
		//turn right
		if (Input.GetKey (KeyCode.RightShift)) {
			nosSpeed = nsfast;
			//sr.color = Color.red;
			//SCRAPPED
		} else {
			nosSpeed = nsnormal;
			//sr.color = Color.black;
			//SCRAPPED
		}
			
	}
}
